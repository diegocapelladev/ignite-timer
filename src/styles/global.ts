import {
  createGlobalStyle,
  css,
  DefaultTheme,
  GlobalStyleComponent
} from 'styled-components'

// eslint-disable-next-line @typescript-eslint/ban-types
type GlobalStylesProps = {}

const GlobalStyles: GlobalStyleComponent<
  GlobalStylesProps,
  DefaultTheme
> = createGlobalStyle`

  * {
    margin: 0;
    padding: 0;
    box-sizing: border-box;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;

    &::before,
    &::after {
      box-sizing: inherit;
    }
  }

  ${({ theme }) => css`

    :focus {
      outline: none;
      box-shadow: 0 0 0 2px ${theme.colors.green500};
    }

    body {
      background-color: ${theme.colors.gray900};
      color: ${theme.colors.gray300};
    }

    body, input, textarea, button {
      font-family: 'Roboto', sans-serif;
      font-weight: 400;
      font-size: 1rem;
    }
  `}
`
export default GlobalStyles
