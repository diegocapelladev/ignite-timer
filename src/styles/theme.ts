export default {
  grid: {
    container: '73.6rem',
    gutter: '3.2rem'
  },
  border: {
    radius: '0.8rem'
  },
  font: {
    family:
      "Inter, -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue', sans-serif",
    light: 300,
    normal: 400,
    bold: 600,
    sizes: {
      xsmall: '1.2rem',
      small: '1.4rem',
      medium: '1.6rem',
      large: '1.8rem',
      xlarge: '2.0rem',
      xxlarge: '2.8rem',
      huge: '5.2rem'
    }
  },
  colors: {
    white: '#FFFFFF',
    black: '#000000',

    gray100: '#E1E1E6',
    gray300: '#C4C4CC',
    gray400: '#8D8D99',
    gray500: '#7C7C8A',
    gray600: '#323238',
    gray700: '#29292E',
    gray800: '#202024',
    gray900: '#121214',

    green300: '#00B37E',
    green500: '#00875F',
    green700: '#015F43',

    red500: '#AB222E',
    red700: '#7A1921',

    yellow500: '#FBA94C'
  },
  spacings: {
    xxsmall: '0.8rem',
    xsmall: '1.6rem',
    small: '2.4rem',
    medium: '3.2rem',
    large: '4.0rem',
    xlarge: '4.8rem',
    xxlarge: '5.6rem'
  }
} as const
