import styled, { css } from 'styled-components'

export const Wrapper = styled.header`
  display: flex;
  align-items: center;
  justify-content: space-between;
`

export const Navbar = styled.nav`
  ${({ theme }) => css`
    display: flex;
    gap: 0.5rem;

    a {
      display: flex;
      align-items: center;
      justify-content: center;
      width: 3rem;
      height: 3rem;
      color: ${theme.colors.gray100};
      border-top: 3px solid transparent;
      border-bottom: 3px solid transparent;

      &:hover {
        border-bottom: 3px solid ${theme.colors.green500};
      }

      &.active {
        color: ${theme.colors.green500};
      }
    }
  `}
`
