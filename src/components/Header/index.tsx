import { Scroll, Timer } from 'phosphor-react'
import { NavLink } from 'react-router-dom'

import Logo from '../../assets/logo-ignite.svg'

import * as S from './styles'

export const Header = () => (
  <S.Wrapper>
    <span>
      <img src={Logo} alt="" />
    </span>

    <S.Navbar>
      <NavLink to="/" title="Timer">
        <Timer size={24} />
      </NavLink>
      <NavLink to="/history" title="Histórico">
        <Scroll size={24} />
      </NavLink>
    </S.Navbar>
  </S.Wrapper>
)
