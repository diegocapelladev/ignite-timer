import { formatDistanceToNow } from 'date-fns'
import { ptBR } from 'date-fns/locale'
import { useContext } from 'react'
import { CyclesContext } from '../../context/CycleContext'
import * as S from './styles'

export const History = () => {
  const { cycles } = useContext(CyclesContext)

  return (
    <S.Wrapper>
      <h1>Meu Histórico</h1>

      <S.HistoryList>
        <S.Table>
          <thead>
            <tr>
              <th>Tarefa</th>
              <th>Duração</th>
              <th>Início</th>
              <th>Status</th>
            </tr>
          </thead>
          <tbody>
            {cycles.map((cycle) => {
              return (
                <tr key={cycle.id}>
                  <td>{cycle.task}</td>
                  <td>{cycle.minutesAmount} minutos</td>
                  <td>
                    {formatDistanceToNow(new Date(cycle.startDate), {
                      addSuffix: true,
                      locale: ptBR
                    })}
                  </td>
                  <td>
                    {cycle.finishedDate && (
                      <S.Status statusColor="green500">Concluído</S.Status>
                    )}

                    {!cycle.finishedDate && !cycle.interruptedDate && (
                      <S.Status statusColor="yellow500">em andamento</S.Status>
                    )}

                    {cycle.interruptedDate && (
                      <S.Status statusColor="red500">Interrompido</S.Status>
                    )}
                  </td>
                </tr>
              )
            })}
          </tbody>
        </S.Table>
      </S.HistoryList>
    </S.Wrapper>
  )
}
