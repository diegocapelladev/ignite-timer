import styled, { css } from 'styled-components'

type statusColorProps = {
  statusColor: 'green500' | 'yellow500' | 'red500'
}

export const Wrapper = styled.main`
  ${({ theme }) => css`
    flex: 1;
    display: flex;
    flex-direction: column;
    padding: 3.5rem;

    h1 {
      font-size: 1.5rem;
      color: ${theme.colors.gray100};
    }
  `}
`

export const HistoryList = styled.div`
  flex: 1;
  overflow: auto;
  margin-top: 2rem;
`

export const Table = styled.table`
  ${({ theme }) => css`
    width: 100%;
    border-collapse: collapse;
    min-width: 600px;

    th {
      background-color: ${theme.colors.gray600};
      color: ${theme.colors.gray100};
      padding: 1rem;
      text-align: left;
      font-size: 0.875rem;
      line-height: ${theme.spacings.xsmall};

      &:first-child {
        border-top-left-radius: ${theme.border.radius};
        padding-left: 1.5rem;
      }


      &:last-child {
        border-top-right-radius: ${theme.border.radius};
        padding-right: 1.5rem;
      }
    }

    td {
      background-color: ${theme.colors.gray700};
      border-top: 4px solid ${theme.colors.gray800};
      line-height: ${theme.spacings.xsmall};
      padding: 1rem;
      font-size: 0.875rem;

      &:first-child {
        width: 50%;
        padding-left: 1.5rem;
      }

      &:last-child {
        padding-right: 1.5rem;
      }
    }
  `}
`

export const Status = styled.span<statusColorProps>`
  ${({ theme, statusColor }) => css`
    display: flex;
    align-items: center;
    gap: 0.5rem;


    &::before {
      content: '';
      width: 0.5rem;
      height: 0.5rem;
      border-radius: 50%;

      background: ${theme.colors[statusColor]};
    }
  `}
`
