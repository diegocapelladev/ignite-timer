import styled, { css } from 'styled-components'

export const Wrapper = styled.main`
  flex: 1;
  display: flex;
  align-items: center;
  justify-content: center;
`
export const Form = styled.form`
  display: flex;
  flex-direction: column;
  align-items: center;
  gap: 3.5rem;
`

export const BaseCountdownButton = styled.button`
  ${({ theme }) => css`
    display: flex;
    align-items: center;
    justify-content: center;
    gap: 0.5rem;

    width: 100%;
    border: 0;
    padding: 1rem;
    border-radius: 8px;
    color: ${theme.colors.gray100};
    font-weight: bold;
    cursor: pointer;

    &:disabled {
      opacity: 0.7;
      cursor: not-allowed;
    }
  `}
`
export const StartCountdownButton = styled(BaseCountdownButton)`
  ${({ theme }) => css`
    background: ${theme.colors.green500};
    color: ${theme.colors.gray100};

    &:not(:disabled):hover {
      background: ${theme.colors.green700};
    }
  `}
`

export const StopCountdownButton = styled(BaseCountdownButton)`
  ${({ theme }) => css`
    background: ${theme.colors.red500};

    &:not(:disabled):hover {
      background: ${theme.colors.red700};
    }
  `}
`
