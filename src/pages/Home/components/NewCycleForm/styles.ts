import styled, { css } from 'styled-components'

export const Wrapper = styled.div`
  ${({ theme }) => css`
    display: flex;
    align-items: center;
    justify-content: center;
    flex-wrap: wrap;
    gap: 0.5rem;

    width: 100%;
    font-size: 1.125rem;
    font-weight: bold;
    color: ${theme.colors.gray100};
  `}
`
const BaseInput = styled.input`
  ${({ theme }) => css`
    height: 2.4rem;
    background: transparent;
    color: ${theme.colors.gray100};
    border: 0;
    border-bottom: 2px solid ${theme.colors.gray500};
    font-weight: bold;
    font-size: 1.125rem;
    padding: 0 0.5rem;

    &:focus {
      box-shadow: none;
      border-color: ${theme.colors.green500};
    }

    &::placeholder {
      color: ${theme.colors.gray500};
    }
  `}
`
export const TaskInput = styled(BaseInput)`
  flex: 1;
  &::-webkit-calendar-picker-indicator {
    display: none !important;
  }
`

export const MinuteAmountInput = styled(BaseInput)`
  width: 4rem;
`
