import styled, { css } from 'styled-components'

export const Wrapper = styled.div`
  ${({ theme }) => css`
    display: flex;
    gap: 1rem;

    font-size: 10rem;
    font-family: 'Roboto Mono', monospace;
    line-height: 8rem;
    color: ${theme.colors.gray100};
  `}
`

export const Number = styled.span`
  ${({ theme }) => css`
    background: ${theme.colors.gray700};
    border-radius: ${theme.border.radius};
    padding: 2rem 1rem;
  `}
`

export const Separator = styled.div`
  ${({ theme }) => css`
    display: flex;
    justify-content: center;

    color: ${theme.colors.green500};
    width: 4rem;
    padding: 2rem 0;
    overflow: hidden;
  `}
`
