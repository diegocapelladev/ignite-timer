import { BrowserRouter } from 'react-router-dom'
import { CyclesContextProvider } from './context/CycleContext'

import { Router } from './Router'

export function App() {
  return (
    <BrowserRouter>
      <CyclesContextProvider>
        <Router />
      </CyclesContextProvider>
    </BrowserRouter>
  )
}
